package segunda;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exemplo1 {

    public static int lerNumero(String texto) {
        Scanner scanner = new Scanner(System.in);
        boolean valido = false;

        int numero = 0;
        do {
            try {
                System.out.print(texto);
                numero = scanner.nextInt();
                valido = true;
            } catch (InputMismatchException ex) {
                scanner.next();
                System.out.println("Erro na conversao. Digite somente inteiros");
            }
        } while (!valido);

        return numero;

    }

    public static int dividir(int a, int b) throws Exception {

        return a / b;
    }

    public static void main(String[] args) {

        int a;
        int b;
        int c = 0;
        try {
            a = lerNumero("A:");
            b = lerNumero("B:");
            c = dividir(a, b);
            System.out.println("C:" + c);
        } catch (Exception ex) {
            System.out.println("Nao é possivel divisao por zero ...");
        }

    }

}
