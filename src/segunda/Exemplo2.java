package segunda;

public class Exemplo2 {

    public static void main(String[] args) {
        f1();
    }

    public static void f1() {
        f2();
    }

    public static void f2() {
        try {
            f3();
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
            System.out.println("tratei ...");
        }
    }

    public static void f3() {
        f4();
    }

    public static void f4() {
        f5();
    }

    public static void f5() {
        throw new RuntimeException("Excecao criada na funcao 5");
    }

}
