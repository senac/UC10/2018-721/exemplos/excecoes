
public class ExemploExcecao {

    public static void main(String[] args) {

        int[] n = criarArray();

    }

    public static int[] criarArray() {
        int[] numeros = null;

        try {
            numeros = new int[3];
            numeros[0] = 15;
            numeros[1] = 45;
            numeros[2] = 456;
            numeros[3] = 789;
            numeros[4] = 7;

        } catch (ArrayIndexOutOfBoundsException ex) {
               System.err.println("Erro ao criar o seu array !!!!!!!!!!!!!!!");
               numeros = null ; 
        }

        return numeros;
    }

}
