
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Exemplo3 {

    public static void main(String[] args) {

        try {
            File file = new File("C:\\AS\\teste.txt");

            InputStreamReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            System.out.println("Abri arquivo .......");
            while (br.ready()) {
                System.out.println(br.readLine());
                throw new Exception("deu ruim !!!!");
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Arquivo nao existe ......");
        } catch (IOException ex) {
            System.out.println("Exemplo3.main()");
        } catch (Exception ex) {
            System.out.println("Erro de operacao");
        }finally{
            System.out.println("Fechar arquivo .......");
        }

    }

}
