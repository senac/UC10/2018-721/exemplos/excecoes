
public class Conta {

    private double saldo;

    public double obterSaldo() {
        return saldo;
    }

    public void adicionarValor(double valor) throws Exception {

        if(valor < 0){
            throw new Exception("Valor negativos nao permitido....");
        }
        
        saldo+= valor;
       
    }

}
